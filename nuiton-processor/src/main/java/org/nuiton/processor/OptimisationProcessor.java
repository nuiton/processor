/*
 * #%L
 * Nuiton Processor :: Api
 * %%
 * Copyright (C) 2002 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/* *
* OptimisationProcessor.java
*
* Created: Wed Sep  4 2002
*
* @author  &lt;poussin@codelutin.com&gt;
* Copyright Code Lutin
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.processor;

import org.nuiton.processor.filters.OptimisationFilter;

import java.io.IOException;

public class OptimisationProcessor extends Processor { // OptimisationProcessor

    public OptimisationProcessor() {
        setInputFilter(new OptimisationFilter());
    }

    /**
     * @param filein   source to process
     * @param fileout  result to write
     * @param encoding encoding to use to read and write files
     * @throws IOException if any io problem while processing
     * @since 1.0.4
     */
    static public void process(String filein, String fileout, String encoding) throws IOException {
        Processor processor = new OptimisationProcessor();
        ProcessorUtil.doProcess(processor, filein, fileout, encoding);
    }

    public static void main(String[] args) throws IOException {
        int length = args.length;
        switch (length) {
            case 0:
            case 1:
                System.out.println("Give source and destination file (and optional encoding)");
                break;
            case 2:
                process(args[0], args[1], ProcessorUtil.DEFAULT_ENCODING);
                break;
            case 3:
                process(args[0], args[1], args[2]);
        }
    }

} // OptimisationProcessor
