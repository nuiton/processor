/*
 * #%L
 * Nuiton Processor :: Api
 * %%
 * Copyright (C) 2002 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/* *
 * ProcessorReader.java
 *
 * Created: Wed Jan 14 2004
 *
 * @author  &lt;poussin@codelutin.com&gt;
 * Copyright Code Lutin
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : */
package org.nuiton.processor;

import org.nuiton.processor.filters.Filter;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;

/**
 * ProcessorReader is used in conjonction with filter to change source data
 *
 * @author poussin
 */
public class ProcessorReader extends LineNumberReader {

    protected LineNumberReader input;

    protected Filter filter;

    protected String lineSeparator;

    protected ProcessorReader() {
        super(new StringReader(""));
    }

    /**
     * Override to send call to input.
     *
     * @return the actual line number.
     */
    @Override
    public int getLineNumber() {
        return input.getLineNumber();
    }

    /** Override to send call to input */
    @Override
    public void setLineNumber(int lineNumber) {
        input.setLineNumber(lineNumber);
    }

    public ProcessorReader(LineNumberReader input, Filter filter) {
        this();
        setInput(input);
        setFilter(filter);
        lineSeparator = System.getProperty("line.separator");
    }

    public void setInput(LineNumberReader input) {
        this.input = input;
    }

    public void setFilter(Filter filter) {
        this.filter = filter;
        if (filter != null) {
            filter.setReader(this);
        }
    }

    @Override
    public boolean markSupported() {
        return false;
    }

    @Override
    public String readLine() throws IOException {
        if (input.ready()) {
            String line = input.readLine();
            if (line != null) {
                if (!(input instanceof ProcessorReader)) {
                    line += lineSeparator;
                }
                String parse = filter.parse(line);
                return parse;
            }
        }
        if (filter.hasCachedData()) {
            return filter.flush();
        }
        return null;
    }

    @Override
    public boolean ready() {
        try {
            boolean ready = input.ready();
            if (ready) {
                return true;
            }
            return filter.hasCachedData();
        } catch (IOException eee) {
            return false;
        }
    }

    @Override
    public void close() throws IOException {
        input.close();
    }
}
