/*
 * #%L
 * Nuiton Processor :: Ant task
 * %%
 * Copyright (C) 2009 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.processor.ant;

import org.apache.commons.lang.StringUtils;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.MatchingTask;
import org.nuiton.processor.Processor;
import org.nuiton.processor.ProcessorUtil;

import java.io.File;
import java.io.IOException;

/**
 * Tache ant pour lutinprocessor.
 *
 * To use this task, put this code in ant build file:
 * <pre>
 * &lt;taskdef name="processor" classname="org.nuiton.processor.ant.ProcessorTask"
 *      classpath="lib/lutinprocessor.jar" /&gt;
 * </pre>
 *
 * and use it with:
 * <pre>
 * &lt;processor srcdir="${src}" destdir="${targetgen}" filters="org.nuiton.processor.filters.GeneratorTemplatesFilter" /&gt;
 * </pre>
 * Created: 14 janv. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt; Copyright Code Lutin
 * @version $Revision$
 *
 * Mise a jour: $Date$ par :
 */
public class ProcessorTask extends MatchingTask { // ProcessorTask

    public static final int MSG_VERBOSE = Project.MSG_VERBOSE;

    private static final String[] EMPTY_STRING_ARRAY = new String[]{};

    protected File srcDir;

    protected File destDir;

    protected String[] includes = EMPTY_STRING_ARRAY;

    protected String[] excludes = EMPTY_STRING_ARRAY;

    protected String[] files;

    protected String fileInPattern = "";

    protected String fileOutPattern = "";

    protected String filters = "org.nuiton.processor.filters.NoActionFilter";

    protected String encoding = ProcessorUtil.DEFAULT_ENCODING;

    protected boolean overwrite = true;

    protected boolean verbose;

    public ProcessorTask() {
    }

    protected String applyTransformationFilename(String filename) {
        return filename.replaceAll(fileInPattern, fileOutPattern);
    }

    protected void doExecute() throws BuildException {
        Processor processor;
        try {
            processor = ProcessorUtil.newProcessor(filters, ",");
        } catch (Exception ex) {
            throw new BuildException(
                    "Could nto instanciate processor for reason : " +
                    ex.getMessage(), ex);
        }
        if (StringUtils.isEmpty(encoding)) {
            setEncoding(ProcessorUtil.DEFAULT_ENCODING);
            log("Using default system encoding " + encoding);
        }
        int numberFiles;
        for (numberFiles = 0; numberFiles < files.length; numberFiles++) {
            String inputFileName = absoluteSourceName(files[numberFiles]);
            String outputFileName = absoluteDestinationName(
                    applyTransformationFilename(files[numberFiles]));
            if (verbose) {
                log("Using " + inputFileName);
            }
            if (overwrite || isNewer(inputFileName, outputFileName)) {
                if (verbose) {
                    log("Generating " + outputFileName);
                }
                // creation du repertoire pour le fichier destination
                File parentFile = new File(outputFileName).getParentFile();
                if (!parentFile.exists()) {
                    boolean b = parentFile.mkdirs();
                    if (!b) {
                        throw new BuildException(
                                "could not create directory " + parentFile);
                    }
                }
                try {
                    ProcessorUtil.doProcess(processor,
                                            inputFileName,
                                            outputFileName,
                                            encoding);
                } catch (IOException eee) {
                    throw new BuildException(eee);
                }
            }
        }
        log("Generating " + numberFiles + " files to " + destDir);
    }

    @Override
    public void execute() throws BuildException {
        // first of all, make sure that we've got a srcdir
        if (srcDir == null) {
            throw new BuildException("srcdir attribute must be set!",
                                     getLocation());
        }
        if (!srcDir.exists()) {
            throw new BuildException("srcdir \"" + srcDir.getPath() +
                                     "\" does not exist!", getLocation());
        }

        if (destDir == null) {
            destDir = srcDir;
        }
        if (!destDir.isDirectory()) {
            throw new BuildException(
                    "destination directory \"" + destDir +
                    "\" does not exist or is not a directory", getLocation());
        }

        // Build the list of files to compute
        buildFileList();

        // generate the source files
        doExecute();
    }

    protected void buildFileList() {

        // Obtain this list of files within the source directory
        DirectoryScanner ds = getDirectoryScanner(srcDir);

        String[] in = getIncludes();
        if (in.length > 0) {
            ds.setIncludes(in);
        }
        String[] ex = getExcludes();
        if (ex.length > 0) {
            ds.setExcludes(ex);
        }

        ds.scan();

        // on recherche ceux que l'on doit vraiment refaire
        files = ds.getIncludedFiles();
    }

    protected boolean isNewer(String filein, String fileout) {
        boolean result = new File(filein).lastModified() >
                         new File(fileout).lastModified();
        if (result) {
            log(filein + " is newer than " + fileout, MSG_VERBOSE);
        }
        return result;
    }

    protected String[] getExcludes() {
        return excludes;
    }

    protected String[] getIncludes() {
        return includes;
    }

    public void setIncludes(String[] includes) {
        this.includes = includes;
    }

    @Override
    public void setIncludes(String includes) {
        setIncludes(includes.split(","));
    }

    public void setExcludes(String[] excludes) {
        this.excludes = excludes;
    }

    @Override
    public void setExcludes(String excludes) {
        setExcludes(excludes.split(","));
    }

    protected String absoluteDestinationName(String fileName) {
        return destDir.getPath() + File.separator + fileName;
    }

    protected String absoluteSourceName(String fileName) {
        return srcDir.getPath() + File.separator + fileName;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public void setOverwrite(boolean overwrite) {
        this.overwrite = overwrite;
    }

    public void setFileOutPattern(String fileOutPattern) {
        this.fileOutPattern = fileOutPattern;
    }

    public void setFileInPattern(String fileInPattern) {
        this.fileInPattern = fileInPattern;
    }

    public void setSrcdir(File srcDir) {
        this.srcDir = srcDir;
    }

    public void setDestdir(File destDir) {
        this.destDir = destDir;
    }

    public void setFilters(String filters) {
        this.filters = filters;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

} // ProcessorTask
