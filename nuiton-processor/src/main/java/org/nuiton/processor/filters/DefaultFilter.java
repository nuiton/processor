/*
 * #%L
 * Nuiton Processor :: Api
 * %%
 * Copyright (C) 2002 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/*******************************************************************************
 * DefaultFilter.java
 *
 * Created: Wed Sep 4 2002
 *
 * @author &lt;poussin@codelutin.com&gt;Copyright Code Lutin
 *
 * @version $Revision$
 *
 * Mise a jour: $Date$ par : */
package org.nuiton.processor.filters;

import java.io.LineNumberReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * A common astract filter. This classe offer getLineNumber method to know
 * line number currently read from source reader.
 *
 * You must implements the methods :
 * <ul>
 * <li>{@link #getHeader()}</li>
 * <li>{@link #getFooter()}</li>
 * <li>{@link #performInFilter(String)}</li>
 * <li>{@link #performOutFilter(String)}</li>
 * </ul>
 *
 * @author poussin
 */
public abstract class DefaultFilter implements Filter { // DefaultFilter

    public final static int NOT_FOUND = -1;

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private final Log log = LogFactory.getLog(DefaultFilter.class);

    protected LineNumberReader reader;

    /**
     * le buffer interne pour conserve ce qui n'a pas encore été écrit
     */
    protected StringBuffer cachedContent = new StringBuffer();

    /** Type of states */
    protected enum State {

        /** Looking-for-header state */
        SEARCH_HEADER,
        /** Looking-for-footer state */
        SEARCH_FOOTER
    }

    /**
     * l'état interne du filtre
     */
    protected State state = State.SEARCH_HEADER;

    /**
     * methode appele lorsqu'on a la chaine entiere entre le header et le
     * footer.
     *
     * @param ch la chaine trouve
     * @return ce qu'il faut ecrire dans le fichier de sortie
     */
    abstract protected String performInFilter(String ch);

    /**
     * methode appele lorsqu'on a la chaine entiere a l'exterieur du
     * header/footer
     *
     * @param ch la chaine trouve
     * @return ce qu'il faut ecrire dans le fichier de sortie
     */
    abstract protected String performOutFilter(String ch);

    /** @return la chaine du header */
    abstract protected String getHeader();

    /** @return la chaine du footer */
    abstract protected String getFooter();

    public void setReader(LineNumberReader reader) {
        this.reader = reader;
    }

    /**
     * Return line currently read in source reader
     * @return line number or -1 if no reader available
     */
    public int getLineNumber() {
        int result = -1;
        if (reader != null) {
            result = reader.getLineNumber();
        }
        return result;
    }

    public String parse(String input) {
        int matchingIndex;

        // add new input to current content
        cachedContent.append(input);
        String content = cachedContent.toString();

        if (state.equals(State.SEARCH_HEADER)) {
            // Looking for a header
            matchingIndex = getMatchIndexFor(content, getHeader());
            if (matchingIndex != NOT_FOUND) {
                // Header found, return "out" part of it + recursive process
                // of remaining chars
                changeState(State.SEARCH_FOOTER);
                StringBuilder buffer = new StringBuilder();
                String outFilter =
                        performOutFilter(content.substring(0, matchingIndex));
                buffer.append(outFilter);
                buffer.append(parse(content.substring(matchingIndex)));
                return buffer.toString();
            }
            // No header found !
            return EMPTY_STRING;
        }

        if (state.equals(State.SEARCH_FOOTER)) {
            // Looking for a footer
            matchingIndex = getMatchIndexFor(content, getFooter());
            if (matchingIndex != NOT_FOUND) {
                // Footer found, return "in" part of it + recursive process
                // of remaining chars
                changeState(State.SEARCH_HEADER);
                int matchingEndIndex = matchingIndex +
                                       getMatchLengthFor(getFooter());
                StringBuilder buffer = new StringBuilder();
                String headerFootfilter = performHeaderFooterFilter(
                        content.substring(0, matchingEndIndex));
                String inFilter = performInFilter(headerFootfilter);
                buffer.append(inFilter);
                buffer.append(parse(content.substring(matchingEndIndex)));
                return buffer.toString();
            }
            // No footer found !
            return EMPTY_STRING;
        }
        throw new IllegalStateException("state " + state + " is unkwon...");
    }

    public String performHeaderFooterFilter(String ch) {
        return ch.substring(getHeader().length(), ch.length() -
                                                  getFooter().length());
    }

    public int getMatchIndexFor(String input, String sequence) {
        return input.indexOf(sequence);
    }

    public int getMatchLengthFor(String sequence) {
        return sequence.length();
    }

    public boolean hasCachedData() {
        return cachedContent.length() > 0;
    }

    public String flush() {
        String line = cachedContent.toString();
        // Empty the cache,
        cachedContent.setLength(0);
        // and returns its content
        if (state.equals(State.SEARCH_HEADER)) {
            return performOutFilter(line);
        }
        return performInFilter(line);
    }

    protected void changeState(State newState) {
        if (log.isDebugEnabled()) {
            log.debug("change state : <old:" + state + ", new:" +
                      newState + ">");
        }

        state = newState;
        // le changement d'état réinitialise toujours le buffer interne
        cachedContent.setLength(0);
    }
} // DefaultFilter

