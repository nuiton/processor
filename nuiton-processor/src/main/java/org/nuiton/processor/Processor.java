/*
 * #%L
 * Nuiton Processor :: Api
 * %%
 * Copyright (C) 2002 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/* *
 * Processor.java
 *
 * Created: Wed Jan 14 2004
 *
 * @author  &lt;poussin@codelutin.com&gt;
 * Copyright Code Lutin
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : */
package org.nuiton.processor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.processor.filters.Filter;
import org.nuiton.processor.filters.NoActionFilter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.io.Writer;

/**
 * This class is a generic sources processor given
 * a reader class name and a writer class name.
 */
public class Processor {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private final Log log = LogFactory.getLog(Processor.class);

    protected Filter[] inputFilter = new Filter[]{new NoActionFilter()};

    protected Filter outputFilter = new NoActionFilter();

    public Processor() {
    }

    public Processor(Filter[] filters) {
        setInputFilter(filters);
    }

    public void setInputFilter(Filter inFilter) {
        setInputFilter(new Filter[]{inFilter});
    }

    public void setInputFilter(Filter[] inFilter) {
        inputFilter = inFilter;
    }

    public void setOutputFilter(Filter outFilter) {
        outputFilter = outFilter;
    }

    protected BufferedReader getReader(Reader externalInput) {
        LineNumberReader result = new LineNumberReader(externalInput);
        for (Filter anInputFilter : inputFilter) {
            result = new ProcessorReader(result, anInputFilter);
        }
        return result;
    }

    /**
     * Process all available data.
     *
     * @param externalInput  the reader
     * @param externalOutput the writer
     * @throws IOException if any pb
     */
    public void process(Reader externalInput,
                        Writer externalOutput) throws IOException {
        if (log.isTraceEnabled()) {
            log.trace("Debug du process");
        }
        BufferedReader input = getReader(externalInput);
        try {
            BufferedWriter writer = new BufferedWriter(externalOutput);
            try {
                ProcessorWriter output = new ProcessorWriter(writer, outputFilter);
                if (log.isTraceEnabled()) {
                    log.trace("input: " + input);
                    log.trace("output: " + output);
                }

                String line = input.readLine();
                while (line != null) {
                    if (log.isTraceEnabled()) {
                        log.trace("Ligne lu: " + line);
                    }
                    output.writeLine(line);
                    line = input.readLine();
                }
                output.flush();
            } finally {
                writer.close();
            }
        } finally {
            input.close();
        }
    }
}
