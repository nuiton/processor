/*
 * #%L
 * Nuiton Processor :: Api
 * %%
 * Copyright (C) 2002 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.processor.filters;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.codehaus.plexus.util.IOUtil;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nuiton.processor.Processor;
import org.nuiton.processor.ProcessorUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/** @author fdesbois */
public class StringFilterTest {

    static private final Log log = LogFactory.getLog(StringFilterTest.class);

    protected static File basedir;

    protected static File testdir;

    @BeforeClass
    public static void initClass() throws Exception {
        basedir = getBasedir();
        testdir = getFile(basedir, "target", "test-classes", "org", "nuiton",
                          "processor", "result");
        boolean b = testdir.exists() || testdir.mkdirs();
        if (!b) {
            throw new IOException("Could not create directory : " + testdir);
        }
    }

    @Test
    public void testStringFilter() throws Exception {
        String fileName = "StringFilterTest.java2";
        URL resource = getClass().getResource(fileName);
        String in = new File(resource.toURI()).getAbsolutePath();
        File out = getFile(testdir, fileName);
        Processor processor = new Processor();
        processor.setInputFilter(new GeneratorTemplatesFilter());
        String encoding = ProcessorUtil.DEFAULT_ENCODING;
        String content = process(processor, in, out, encoding);
        if (log.isDebugEnabled()) {
            log.debug("output : " + out);
            log.debug(content);
        }
        checkPattern("+\" int i = 0;\\n\"", true, out, encoding);
        fileName = "StringFilterTest_1.java2";
        resource = getClass().getResource(fileName);
        in = new File(resource.toURI()).getAbsolutePath();
        out = getFile(testdir, fileName);
        content = process(processor, in, out, encoding);
        if (log.isDebugEnabled()) {
            log.debug("output : " + out);
            log.debug(content);
        }

        checkPattern("troulala;", true, out, encoding);
    }

    protected String process(Processor processor,
                             String in,
                             File out,
                             String encoding) throws IOException {
        ProcessorUtil.doProcess(processor, new File(in), out, encoding);
        String content = readAsString(out, encoding);
        return content;
    }

    static public String readAsString(File file,
                                      String encoding) throws IOException {
        FileInputStream inf = new FileInputStream(file);
        BufferedReader in =
                new BufferedReader(new InputStreamReader(inf, encoding));
        try {
            return IOUtil.toString(in);
        } finally {
            in.close();
        }
    }

    public static File getFile(File base, String... paths) {
        StringBuilder buffer = new StringBuilder();
        for (String path : paths) {
            buffer.append(File.separator).append(path);
        }
        File f = new File(base, buffer.substring(1));
        return f;
    }

    public static File getBasedir() {
        String basedirPath = System.getProperty("basedir");

        if (basedirPath == null) {
            basedirPath = new File("").getAbsolutePath();
        }

        return new File(basedirPath);
    }

    protected void checkPattern(String pattern,
                                boolean required,
                                File f,
                                String encoding) throws IOException {

        if (log.isDebugEnabled()) {
            log.debug("check generated file " + f);
        }

        Assert.assertTrue("generated file " + f + " was not found...",
                          f.exists()
        );

        String content = readAsString(f, encoding);

        String errorMessage = required ? "could not find the pattern : " :
                              "should not have found pattern :";
        Assert.assertEquals(errorMessage + pattern + " in file " + f,
                            required, content.contains(pattern));
    }
}
