/*
 * #%L
 * Nuiton Processor :: Api
 * %%
 * Copyright (C) 2002 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/* *
 * GeneratorTemplatesFilter.java
 *
 * Created: Wed Sep  4 2002
 *
 * @author  <pineau@codelutin.com>
 * Copyright Code Lutin
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : */

/**
 * MultilinesLitteralsFilter.java
 */

package org.nuiton.processor.filters;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Classe principale du filtre de génération. Ce filtre recherche tout ce qu'il
 * y a entre les tag &#47;*[ et ]*&#47; a l'interieur de ces tags un autre filtre
 * ({@link GeneratorTemplatesFilterIn}) est
 * utilisé pour générer les tags &lt;% %&gt;, &lt;%= %&gt; et *).
 * <ul>
 * <li>&#47;*[ et ]*&#47; est remplacer par + "..." pour chaque
 * ligne et les " sont coté</li>
 * <li>&lt;% %&gt;</li>
 * <li>&lt;%= %&gt;</li>
 * <li>*) permet de fermer un commentaire sans le fermer réeellement, cela
 * permet au éditeur faisant une analyse du code de continuer a fonctionner
 * ce tag est remplacé par son equivalent *&#47; après génération.</li>
 * </ul>
 * Vous pouvez modifier le comportement du processor en mettant des options
 * Une option est incluse dans le tag et est de la forme
 * &#47;*[generator option: &lt;optionName&gt; = valeur]*&#47;
 * les options existantes sont:
 * <ul>
 * <li>passEmptyLine: boolean; cette option permet de supprimer la première
 * et la dernière ligne si elles sont videx</li>
 * <li>writeParentheses: boolean, default: false : cette option permet d'ajouter les
 * parentheses autour du resultat (par defaut ces parentheses correspondent a celles
 * de l'appel a la  methode output.write)</li>
 * <li>wtriteString: String, default: +</li>
 * </ul>
 */
public class GeneratorTemplatesFilterBody extends GeneratorTemplatesFilter {

    private static final Log log =
            LogFactory.getLog(GeneratorTemplatesFilterBody.class);

    public GeneratorTemplatesFilterBody() {
        passEmptyLine = false;
        writeParentheses = false;
        writeString = "+ ";
        inFilter = new GeneratorTemplatesFilterIn(this);
    }

    /** la chaine du header */
    protected String getHeader() {
        return "/*[";
    }

    /** la chaine du footer */
    protected String getFooter() {
        return "]*/";
    }

}
