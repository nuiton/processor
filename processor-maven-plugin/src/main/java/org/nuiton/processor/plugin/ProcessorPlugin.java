/*
 * #%L
 * Nuiton Processor :: Maven plugin
 * 
 * $Id$
 * $HeadURL$
 * %%
 * Copyright (C) 2006 - 2010 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.processor.plugin;

import org.apache.commons.lang.StringUtils;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.nuiton.io.MirroredFileUpdater;
import org.nuiton.plugin.AbstractPlugin;
import org.nuiton.plugin.PluginHelper;
import org.nuiton.plugin.PluginWithEncoding;
import org.nuiton.processor.Processor;
import org.nuiton.processor.ProcessorUtil;

import java.io.File;
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Maven2 plugin for Nuiton Processor.
 *
 * Created: 14 avril 2006
 *
 * @author jruchaud &lt;ruchaud@codelutin.com&gt;
 * @author tchemit &lt;chemit@codelutin.com&gt;
 */
@Mojo(name = "process", requiresProject = true)
public class ProcessorPlugin extends AbstractPlugin implements PluginWithEncoding {

    /**
     * Dependance du projet.
     *
     * @since 1.0.0
     */
    @Component
    protected MavenProject project;

    /**
     * Répertoire source.
     *
     * @since 0.10
     */
    @Parameter(property = "processor.srcDir", defaultValue = "${basedir}/src/main/java")
    protected File srcDir;

    /**
     * Répertoire cible.
     *
     * @since 0.10
     */
    @Parameter(property = "processor.destDir", defaultValue = "${basedir}/target/processed-sources/java")
    protected File destDir;

    /**
     * Fichiers à inclure.
     *
     * Il s'agit des expressions séparées par des virgules.
     *
     * Exemple :
     *
     * *.java,*.xml
     *
     * @since 0.10
     */
    @Parameter(property = "processor.includes")
    protected String includes;

    /**
     * Fichiers à exclure.
     *
     * Il s'agit des expressions spérarées par des virgules.
     *
     * Exemple :
     *
     * *.java,*.xml
     *
     * @since 0.10
     */
    @Parameter(property = "processor.excludes")
    protected String excludes;

    /**
     * TODO
     *
     * @since 0.10
     */
    @Parameter(property = "processor.fileInPattern", defaultValue = "")
    protected String fileInPattern = "";

    /**
     * TODO
     *
     * @since 0.10
     */
    @Parameter(property = "processor.fileOutPattern", defaultValue = "")
    protected String fileOutPattern = "";

    /**
     * Les filtres a utiliser par le processor, séparés par des virgules
     *
     * @since 0.10
     */
    @Parameter(property = "processor.filters",
               defaultValue = "org.nuiton.processor.filters.NoActionFilter")
    protected String filters;

    /**
     * Encoding used to read and writes files.
     *
     * <b>Note:</b> If nothing is filled here, we will use the system
     * property {@code file.encoding}.
     *
     * @since 1.0.4
     */
    @Parameter(property = "processor.encoding",
               defaultValue = "${project.build.sourceEncoding}",
               required = true)
    private String encoding;

    /**
     * Ecrase les fichiers générés.
     *
     * @since 0.10
     */
    @Parameter(property = "processor.overwrite", defaultValue = "false")
    protected boolean overwrite;

    /**
     * Permet d'obtenir plus d'information.
     *
     * @since 0.10
     */
    @Parameter(property = "processor.verbose", defaultValue = "${maven.verbose}")
    protected boolean verbose;

    /**
     * Pour utiliser en phase de test.
     *
     * @since 1.0.3
     */
    @Parameter(property = "processor.testPhase", defaultValue = "false")
    protected boolean testPhase;

    /** Les fichiers à traiter */
    protected Map<String, String> filesToTreate;

    public boolean isVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public MavenProject getProject() {
        return project;
    }

    public void setProject(MavenProject project) {
        this.project = project;
    }

    public final String getEncoding() {
        return encoding;
    }

    public final void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    @Override
    protected void init() throws Exception {

        if (StringUtils.isEmpty(filters)) {
            return;
        }

        MirroredFileUpdater updater = overwrite ? null :
                                      new MirroredFileUpdater(fileInPattern,
                                                              fileOutPattern,
                                                              srcDir,
                                                              destDir
                                      );

        if (updater != null) {
            updater.setDestinationDirectory(destDir);
        }
        String[] aIncludes = includes.split(",");
        String[] aExcludes = excludes == null ? null : excludes.split(",");

        filesToTreate = getFilesToTreate(aIncludes, aExcludes, srcDir, updater);
    }

    @Override
    protected boolean checkSkip() {
        if (StringUtils.isEmpty(filters)) {
            getLog().warn("No filters to use, skip execution.");
            return false;
        }
        if (filesToTreate == null || filesToTreate.isEmpty()) {
            getLog().info("No file to process.");
            return false;
        }
        return true;
    }

    @Override
    protected void doAction() throws Exception {

        if (isVerbose()) {
            printConfig();
        }

        long t0 = System.nanoTime();

        Processor processor = ProcessorUtil.newProcessor(filters, ",");

        getLog().info("Processing " + filesToTreate.size() + " files(s).");

        for (Entry<String, String> entry : filesToTreate.entrySet()) {
            File srcFile = new File(srcDir, entry.getKey());
            File dstFile = new File(entry.getValue());

            if (isVerbose()) {
                getLog().info("Process " + srcFile);
            }
            // creation du repertoire pour le fichier destination
            createDirectoryIfNecessary(dstFile.getParentFile());

            ProcessorUtil.doProcess(processor, srcFile, dstFile, getEncoding());
        }

        if (isVerbose()) {
            long time = System.nanoTime() - t0;
            getLog().info("done in " + PluginHelper.convertTime(time));
        }
        // on indique que le repertoire entrant n'est plus dans le build
        // car sinon on va avoir des classes dupliquées
        if (testPhase) {
            removeTestCompileSourceRoots(srcDir);
        } else {

            removeCompileSourceRoots(srcDir);
        }

        // le repertoire sortant est dans le build de maven
        if (testPhase) {
            addTestCompileSourceRoots(destDir);
        } else {
            addCompileSourceRoots(destDir);
        }
    }

    protected void printConfig() {
        getLog().info("config - srcDir         " + srcDir);
        getLog().info("config - destDir        " + destDir);
        getLog().info("config - includes       " + includes);
        getLog().info("config - filters        " + Arrays.asList(
                PluginHelper.splitAndTrim(filters, ",")));
        if (excludes != null) {
            getLog().info("config - excludes       " + excludes);
        }
        if (!StringUtils.isEmpty(fileInPattern)) {
            getLog().info("config - fileInPattern  " + fileInPattern);
        }
        if (!StringUtils.isEmpty(fileOutPattern)) {
            getLog().info("config - fileOutPattern " + fileOutPattern);
        }
        if (overwrite) {
            getLog().info("config - overwrite      " + overwrite);
        }
    }
}
