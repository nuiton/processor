/*
 * #%L
 * Nuiton Processor :: Api
 * %%
 * Copyright (C) 2002 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/* x*
* ActiveLogsCodeFilter.java
*
* Created: Wed Jan 14 2004
*
* @author  &lt;poussin@codelutin.com&gt;
* Copyright Code Lutin
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.processor.filters;

/*
* This class deals with the
*/
public class ActiveLogsCodeFilter extends LogsFilter {

    /**
     * methode appele lorsqu'on a la chaine entiere entre le header
     * et le footer.
     *
     * @param ch la chaine trouve
     * @return ce qu'il faut ecrire dans le fichier de sortie
     */
    @Override
    protected String performInFilter(String ch) {
        // Logs must be performed within a try{...}catch{} bloc
        return
                "try {\n" +
                ch +
                "} catch (Exception logsE) {\n" +
                "    System.err.println(\"Error in Logging instructions\");\n" +
                "    logsE.printStackTrace();\n" +
                "}";
    }

    /**
     * methode appele lorsqu'on a la chaine entiere a l'exterieur du
     * header/footer
     *
     * @param ch la chaine trouve
     * @return ce qu'il faut ecrire dans le fichier de sortie
     */
    @Override
    protected String performOutFilter(String ch) {
        return ch;
    }

}
