/*
 * #%L
 * Nuiton Processor :: Api
 * %%
 * Copyright (C) 2002 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/**
 * ProcessorWriter.java
 *
 * Created: Wed Jan 14 2004
 *
 * @author  &lt;poussin@codelutin.com&gt;
 * Copyright Code Lutin
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : $Author$
 */

package org.nuiton.processor;

import org.nuiton.processor.filters.Filter;

import java.io.BufferedWriter;
import java.io.IOException;

public class ProcessorWriter {

    protected BufferedWriter output;

    protected Filter filter;

    public ProcessorWriter(BufferedWriter output, Filter filter) {
        this.output = output;
        this.filter = filter;
    }

    public void writeLine(String line) throws IOException {

        // TODO Euh on se sert quand du filtre de sortie ???? :-)
        output.write(line);
    }

    public void newLine() throws IOException {
        output.newLine();
    }

    public void flush() throws IOException {
        output.flush();
    }

    public void close() throws IOException {
        output.close();
    }

}
