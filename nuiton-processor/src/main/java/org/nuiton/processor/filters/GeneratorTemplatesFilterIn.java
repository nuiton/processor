/*
 * #%L
 * Nuiton Processor :: Api
 * %%
 * Copyright (C) 2002 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/* *
 * GeneratorTemplatesFilterIn.java
 *
 * Created: 14 janv. 2004
 *
 * @author Benjamin Poussin &lt;poussin@codelutin.com&gt;
 * Copyright Code Lutin
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : */

package org.nuiton.processor.filters;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Le tag *) n'est substitué que s'il est a l'extérieur du tag &lt;%=...%&gt;ou
 * &lt;%...%&gt;
 */
public class GeneratorTemplatesFilterIn extends DefaultFilter { // GeneratorTemplatesFilterIn

    private static final Log log =
            LogFactory.getLog(GeneratorTemplatesFilterIn.class);

    GeneratorTemplatesFilter parent;

    protected String beginParenthese = EMPTY_STRING;

    protected String endParenthese = EMPTY_STRING;

    protected String closingWriterChar;

    protected String openingWriterChar;

    protected String endCar;

    protected String stringDelim;

    protected String concacChar;

    public String getConcacChar() {
        return concacChar;
    }

    public void setConcacChar(String concacChar) {
        this.concacChar = concacChar;
    }

    public String getStringDelim() {
        return stringDelim;
    }

    public void setStringDelim(String stringDelim) {
        this.stringDelim = stringDelim;
    }

    public String getEndCar() {
        return endCar;
    }

    public void setEndCar(String endCar) {
        this.endCar = endCar;
    }

    public String getEndParenthese() {
        return endParenthese;
    }

    public void setEndParenthese(String endParenthese) {
        this.endParenthese = endParenthese;
    }

    public String getBeginParenthese() {
        return beginParenthese;
    }

    public void setBeginParenthese(String beginParenthese) {
        this.beginParenthese = beginParenthese;
    }

    public GeneratorTemplatesFilter getParent() {
        return parent;
    }

    public void setParent(GeneratorTemplatesFilter parent) {
        this.parent = parent;
    }

    public String getClosingWriterChar() {
        return closingWriterChar;
    }

    public void setClosingWriterChar(String closingWriterChar) {
        this.closingWriterChar = closingWriterChar;
    }

    public String getOpeningWriterChar() {
        return openingWriterChar;
    }

    public void setOpeningWriterChar(String openingWriterChar) {
        this.openingWriterChar = openingWriterChar;
    }

    public GeneratorTemplatesFilterIn(GeneratorTemplatesFilter parent) {
        this.parent = parent;
        endCar=";";
        stringDelim="\"";
        concacChar="+";
        openingWriterChar ="(";
        closingWriterChar = ")";
    }

    /**
     * méthode appelée lorsqu'on a la chaîne entière entre le header et le
     * footer.
     *
     * @param ch la chaîne trouvé
     * @return ce qu'il faut écrire dans le fichier de sortie
     */
    @Override
    protected String performInFilter(String ch) {
        if (parent.writeParentheses) {
            beginParenthese = openingWriterChar;
            endParenthese = closingWriterChar+ endCar;
        } else {
            beginParenthese = EMPTY_STRING;
            endParenthese = EMPTY_STRING;
        }
        if (ch.startsWith("=")) {
            return stringDelim + concacChar + ch.substring(1) + concacChar + stringDelim;
        }
        return stringDelim + endParenthese  + ch + parent.getWriteString() +
               beginParenthese + stringDelim;
    }

    /**
     * Converti les fin de commentaire *) en fin normal de commentraire
     *
     * @param ch la chaine a convertir
     * @return la chaine convertie
     */
    protected String convertEndComment(String ch) {
        String result = ch.replaceAll("\\*\\)", "*/");
        return result;
    }

    /**
     * methode appele lorsqu'on a la chaine entiere a l'exterieur du
     * header/footer
     *
     * @param ch la chaine trouve
     * @return ce qu'il faut ecrire dans le fichier de sortie
     */
    @Override
    protected String performOutFilter(String ch) {
        if (parent.writeParentheses) {
            beginParenthese = openingWriterChar;
            endParenthese = closingWriterChar+ endCar;
        } else {
            beginParenthese = EMPTY_STRING;
            endParenthese = EMPTY_STRING;
        }
        String result = convertEndComment(ch).replaceAll("\"", "\\\\\"")
                .replaceAll("(\r\n|\n|\r)",
                            "\\\\n"+ stringDelim + endParenthese  + "\n" +
                            parent.getWriteString() + beginParenthese + stringDelim);
        // it's important that \r\n is first in regexp.

        return result;
    }

    @Override
    protected String getHeader() {
        return "<%";
    }

    @Override
    protected String getFooter() {
        return "%>";
    }

} // GeneratorTemplatesFilterIn

