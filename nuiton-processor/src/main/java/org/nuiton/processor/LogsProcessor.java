/*
 * #%L
 * Nuiton Processor :: Api
 * %%
 * Copyright (C) 2002 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/* *
* LogsProcessor.java
*
* Created: Wed Jan 14 2004
*
* @author  &lt;poussin@codelutin.com&gt;
* Copyright Code Lutin
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.processor;

import org.nuiton.processor.filters.ActiveLogsCodeFilter;
import org.nuiton.processor.filters.RemoveLogsCodeFilter;

/*
* This class is a processor for source logs
*/
public class LogsProcessor extends Processor {
    private static final String REMOVE_ACTION = "remove";

    private static final String ACTIVE_ACTION = "active";

    public enum Action {
        NoAction,
        Logs,
        NoLogsCode
    }

    public LogsProcessor(Action action) {
        switch (action) {

            case NoAction:
                // No action is the default filter in Processor
                break;

            case Logs:
                // Go for logs
                setInputFilter(new ActiveLogsCodeFilter());
                break;

            case NoLogsCode:
                setInputFilter(new RemoveLogsCodeFilter());
                break;
        }
    }

    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            System.out.println("Give source and destination file, then action");
            System.out.println("Action may be remove or active (default is no action)");
            return;
        }
        Action action = Action.NoAction;
        if (args.length > 2) {
            if (args[2].equals(REMOVE_ACTION)) {
                action = Action.NoLogsCode;
                System.out.println("Removing logs code");
            } else if (args[2].equals(ACTIVE_ACTION)) {
                action = Action.Logs;
                System.out.println("Setting logs active");
            }
        }

        if (action == Action.NoAction) {
            System.out.println("No action taken");
        }

        LogsProcessor processor = new LogsProcessor(action);
        ProcessorUtil.doProcess(processor, args[0], args[1], ProcessorUtil.DEFAULT_ENCODING);
    }
}
