/*
 * #%L
 * Nuiton Processor :: Api
 * %%
 * Copyright (C) 2002 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/* *
 * GeneratorTemplatesFilter.java
 *
 * Created: Wed Sep  4 2002
 *
 * @author  <pineau@codelutin.com>
 * Copyright Code Lutin
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : */

package org.nuiton.processor.filters;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Classe principale du filtre de génération. Ce filtre recherche tout ce qu'il
 * y a entre les tag &#47;*{ et }*&#47; a l'interieur de ces tags un autre filtre
 * ({@link org.nuiton.processor.filters.GeneratorTemplatesFilterIn}) est
 * utilisé pour générer les tags &lt;% %&gt;, &lt;%= %&gt; et *).
 * <ul>
 *  <li>&#47;*{ et }*&#47; est remplacer par output.write("..."); pour chaque
 * ligne et les " sont coté</li>
 *  <li>&lt;% %&gt;</li>
 *  <li>&lt;%= %&gt;</li>
 *  <li>*) permet de fermer un commentaire sans le fermer réeellement, cela
 *      permet au éditeur faisant une analyse du code de continuer a fonctionner
 *      ce tag est remplacé par son equivalent *&#47; après génération.</li>
 * </ul>
 * Vous pouvez modifier le comportement du processor en mettant des options
 * Une option est incluse dans le tag et est de la forme
 * &#47;*{generator option: &lt;optionName&gt; = valeur}*&#47;
 * les options existantes sont:
 * <ul>
 * <li>passEmptyLine: boolean; cette option permet de supprimer la première
 * et la dernière ligne si elles sont videx</li>
 * <li>writeParentheses: boolean, default: true : cette option permet d'ajouter les
 * parentheses autour du resultat (par defaut ces parentheses correspondent a celles
 * de l'appel a la  methode output.write)</li>
 * <li>wtriteString: String, default: output.write</li>
 * </ul>
 */
public class GeneratorTemplatesFilter extends DefaultFilter {

    private static final Log log =
            LogFactory.getLog(GeneratorTemplatesFilter.class);

    protected boolean passEmptyLine;

    protected boolean writeParentheses;

    protected String writeString;

    protected GeneratorTemplatesFilterIn inFilter;

    public GeneratorTemplatesFilter() {
        passEmptyLine = false;
        writeParentheses = true;
        writeString = "output.write";
        inFilter = new GeneratorTemplatesFilterIn(this);
    }

    public String getWriteString() {
        return writeString;
    }

    public void setWriteString(String writeString) {
        this.writeString = writeString;
    }

    public boolean isPassEmptyLine() {
        return passEmptyLine;
    }

    public void setPassEmptyLine(boolean passEmptyLine) {
        this.passEmptyLine = passEmptyLine;
    }

    public boolean isWriteParentheses() {
        return writeParentheses;
    }

    public void setWriteParentheses(boolean writeParentheses) {
        this.writeParentheses = writeParentheses;
    }

    public GeneratorTemplatesFilterIn getInFilter() {
        return inFilter;
    }

    public void setInFilter(GeneratorTemplatesFilterIn inFilter) {
        this.inFilter = inFilter;
    }

    /**
     * methode appele lorsqu'on a la chaine entiere entre le header
     * et le footer.
     *
     * @param ch la chaine trouve
     * @return ce qu'il faut ecrire dans le fichier de sortie
     */
    protected String performInFilter(String ch) {
        if (ch.matches("generator option: *passEmptyLine *= *(true|false)")) {
            passEmptyLine =
                    "true".equalsIgnoreCase(ch.substring(ch.length() - 4));
            return EMPTY_STRING;
        } else if (ch.matches("generator option: *writeString *= *.*")) {
            writeString =
                    ch.replaceAll("generator option: *writeString *= *(.*)", "$1");
            return EMPTY_STRING;
        } else if (ch.matches("generator option: *parentheses *= *(true|false)")) {
            writeParentheses =
                    "true".equalsIgnoreCase(ch.substring(ch.length() - 4));
            return EMPTY_STRING;
        } else {
            if (passEmptyLine && ch.length() > 0) {
                // suppression des premieres et dernieres lignes si elles sont vides
                String[] all = ch.split("\n");
                if (all.length > 0) {
                    // si la derniere ne match que des blancs
                    if (all[all.length - 1].matches("\\s*")) {
                        int len = ch.length() - all[all.length - 1].length() - 1;
                        ch = ch.substring(0, Math.max(len, 0));
                    }

                    // suppression de la 1er ligne si elle est vide
                    if (all[0].matches("\\s*")) {
                        int len = all[0].length() + 1;
                        ch = ch.substring(Math.min(ch.length(), len));
                    }
                }
            }

            String resultInFilter = inFilter.parse(ch) + inFilter.flush();

            String result = getWriteString();
            if (writeParentheses) {
                result += "(";
            }
            result += "\"" + resultInFilter + "\"";
            if (writeParentheses) {
                result += ");";
            }

            if (log.isDebugEnabled()) {
                log.debug("writeParentheses = " + writeParentheses);
            }
            return result;
        }
    }

    /**
     * methode appele lorsqu'on a la chaine entiere a l'exterieur du
     * header/footer
     *
     * @param ch la chaine trouve
     * @return ce qu'il faut ecrire dans le fichier de sortie
     */
    protected String performOutFilter(String ch) {
        return ch;
    }


    /** la chaine du header */
    protected String getHeader() {
        return "/*{";
    }

    /** la chaine du footer */
    protected String getFooter() {
        return "}*/";
    }

    public static void main(String[] args) {
        Filter filter = new GeneratorTemplatesFilter();
        String res = filter.parse("   public void generatePackageStatement(Writer output, ObjectModelClassifier clazz) throws IOException {\n/*{\n package <%=clazz.getPackageName()%>.persistence.jdo; \n}*/ }");
        System.out.println(res);
        res = filter.flush();
        System.out.println(res);
    }


}
