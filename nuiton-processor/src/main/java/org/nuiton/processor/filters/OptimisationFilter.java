/*
 * #%L
 * Nuiton Processor :: Api
 * %%
 * Copyright (C) 2002 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/* *
* OptimisationFilter.java
*
* Created: Wed Sep  4 2002
*
* @author  &lt;poussin@codelutin.com&gt;
* Copyright Code Lutin
* @version $Revision$
*
* Mise a jour: $Date$
* par : $Author$
*/

package org.nuiton.processor.filters;

public class OptimisationFilter extends DefaultFilter { // OptimisationFilter

    public OptimisationFilter() {
    }

    private String header = "/*%";

    private String footer = "%*/";

    protected String getHeader() {
        return header;
    }

    protected String getFooter() {
        return footer;
    }

    /**
     * methode appele lorsqu'on a la chaine entiere entre le header
     * et le footer.
     *
     * @param ch la chaine trouve
     * @return ce qu'il faut ecrire dans le fichier de sortie
     */
    protected String performInFilter(String ch) {
        return ch;
    }

    /**
     * methode appele lorsqu'on a la chaine entiere a l'exterieur du
     * header/footer
     *
     * @param ch la chaine trouve
     * @return ce qu'il faut ecrire dans le fichier de sortie
     */
    protected String performOutFilter(String ch) {
        return EMPTY_STRING;
    }

} // OptimisationFilter
