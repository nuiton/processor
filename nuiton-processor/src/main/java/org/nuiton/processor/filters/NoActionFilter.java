/*
 * #%L
 * Nuiton Processor :: Api
 * %%
 * Copyright (C) 2002 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


/* *
 * NoActionFilter.java
 *
 * Created: Wed Jan 14 2004
 *
 * @author  &lt;poussin@codelutin.com&gt;
 * Copyright Code Lutin
 * @version $Revision$
 *
 * Mise a jour: $Date$
 * par : */
package org.nuiton.processor.filters;

import java.io.LineNumberReader;

/**
 * This filter does nothing on input.
 * 
 */
public class NoActionFilter implements Filter {

    public void setReader(LineNumberReader reader) {
    }

    public String parse(String input) {
        return input;
    }

    public boolean hasCachedData() {
        return false;
    }

    public String flush() {
        return EMPTY_STRING;
    }
}
