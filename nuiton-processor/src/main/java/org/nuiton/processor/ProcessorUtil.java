/*
 * #%L
 * Nuiton Processor :: Api
 * %%
 * Copyright (C) 2002 - 2010 CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.processor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.processor.filters.DefaultFilter;
import org.nuiton.processor.filters.Filter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Utilities methods.
 *
 * @author tchemit &lt;chemit@codelutin.com&gt;
 * @since 1.0.0.0
 */
public class ProcessorUtil {

    /** Logger */
    private static final Log log = LogFactory.getLog(ProcessorUtil.class);

    /**
     * System encoding (use as fall back for deprecated method with no given
     * encoding.
     *
     * @since 1.0.4
     */
    public static final String DEFAULT_ENCODING =
            System.getProperty("file.encoding");

    /**
     * Instanciate a new {@link Processor} given an array of filters given by
     * their FQN separated by the given separator.
     *
     * @param filters   the string representionf of the filters
     * @param separator the separator of filter
     * @return the instanciated processor
     * @throws Exception if any pb
     */
    public static Processor newProcessor(String filters,
                                         String separator) throws Exception {
        Filter[] result = getFilters(filters, separator);
        Processor processor = new Processor(result);
        return processor;
    }

    /**
     * Instanciate a array of filters given by thier FQN separated by the
     * given separator.
     *
     * @param filters   the list of filters separated by the given separator
     * @param separator filter separator
     * @return the array of instanciated filters.
     * @throws Exception if any pb
     */
    public static Filter[] getFilters(String filters,
                                      String separator) throws Exception {
        String[] filterList = filters.split(separator);
        Filter[] result = new Filter[filterList.length];
        for (int i = 0; i < filterList.length; i++) {

            try {
                // Class.forName semble fonctionner maintenant
                // avant il fallait utiliser getClass().forName
                result[i] = (Filter)
                        Class.forName(filterList[i].trim()).newInstance();
            } catch (Exception eee) {
                throw new IllegalArgumentException(
                        "Error during looking for '" + filterList[i].trim() +
                        "' class", eee);
            }
        }
        return result;
    }

    /**
     * Launch the process of the given {@code processor} to the given files.
     *
     * @param processor the processor to launch
     * @param in        the input file
     * @param out       the output file
     * @param encoding  encoding to use to read and write files
     * @throws IOException if any problems while processing file
     * @since 1.0.4
     */
    public static void doProcess(Processor processor,
                                 String in,
                                 String out,
                                 String encoding) throws IOException {
        doProcess(processor, new File(in), new File(out), encoding);
    }

    /**
     * Launch the process of the given {@code processor} to the given files.
     *
     * @param processor the processor to launch
     * @param in        the input file
     * @param out       the output file
     * @throws IOException if any problems while processing file
     * @since 1.0.3
     * @deprecated since 1.0.4, prefer use the {@link #doProcess(Processor, String, String, String)}.
     */
    @Deprecated
    public static void doProcess(Processor processor,
                                 String in, String out) throws IOException {
        doProcess(processor, new File(in), new File(out), DEFAULT_ENCODING);
    }

    /**
     * Launch the process of the given {@code processor} to the given files.
     *
     * @param processor the processor to launch
     * @param in        the input file
     * @param out       the output file
     * @param encoding  encoding to use to read and write files.
     * @throws IOException if any problems while processing file
     * @since 1.0.4
     */
    public static void doProcess(Processor processor,
                                 File in,
                                 File out,
                                 String encoding) throws IOException {
        InputStreamReader input = new InputStreamReader(new FileInputStream(in), encoding);
        try {
            OutputStreamWriter output = new OutputStreamWriter(new FileOutputStream(out), encoding);
            try {
                processor.process(input, output);
            } catch (IOException eee) {
                if (log.isErrorEnabled()) {
                    log.error(
                            "Error while processing file " + in + " to " + out +
                            " with processor " + processor, eee);
                }
                throw eee;
            } finally {
                output.close();
            }
        } finally {
            input.close();
        }
    }

    /**
     * Launch the process of the given {@code processor} to the given streams.
     *
     * @param processor the processor to launch
     * @param in        the input file
     * @param out       the output file
     * @param encoding  encoding to use to read and write files.
     * @throws IOException if any problems while processing file
     * @since 1.1
     */
    public static void doProcess(Processor processor,
                                 InputStream in,
                                 OutputStream out,
                                 String encoding) throws IOException {
        InputStreamReader input = new InputStreamReader(in, encoding);
        try {
            OutputStreamWriter output = new OutputStreamWriter(out, encoding);
            try {
                processor.process(input, output);
            } catch (IOException eee) {
                if (log.isErrorEnabled()) {
                    log.error(
                            "Error while processing file " + in + " to " + out +
                            " with processor " + processor, eee);
                }
                throw eee;
            } finally {
                output.close();
            }
        } finally {
            input.close();
        }
    }

    /**
     * Launch the process of the given {@code processor} to the given readers.
     *
     * @param processor the processor to launch
     * @param in        the input file
     * @param out       the output file
     * @throws IOException if any problems while processing file
     * @since 1.1
     */
    public static void doProcess(Processor processor,
                                 InputStreamReader in,
                                 OutputStreamWriter out) throws IOException {
        try {
            processor.process(in, out);
        } catch (IOException eee) {
            if (log.isErrorEnabled()) {
                log.error(
                        "Error while processing file " + in + " to " + out +
                        " with processor " + processor, eee);
            }
            throw eee;
        }
    }

    /**
     * Launch the process of the given {@code processor} to the given files using
     * the system default encoding.
     *
     * @param processor the processor to launch
     * @param in        the input file
     * @param out       the output file
     * @throws IOException if any problems while processing file
     * @since 1.0.3
     * @deprecated since 1.0.4, prefer use the {@link #doProcess(Processor, File, File, String)}.
     */
    @Deprecated
    public static void doProcess(Processor processor,
                                 File in, File out) throws IOException {
        doProcess(processor, in, out, DEFAULT_ENCODING);
    }

    /**
     * To extract all content inside the header and footer.
     *
     * @author tchemit &lt;chemit@codelutin.com&gt;
     * @since 1.1
     */
    public static class FragmentExtractor extends DefaultFilter {

        /** Logger. */
        static protected final Log log =
                LogFactory.getLog(FragmentExtractor.class);

        protected String header;

        protected String footer;

        protected boolean verbose;

        public FragmentExtractor(String header, String footer) {
            this.header = header;
            this.footer = footer;
        }

        public void setVerbose(boolean verbose) {
            this.verbose = verbose;
        }

        @Override
        protected String performInFilter(String ch) {
            if (verbose || log.isInfoEnabled()) {
                log.info("Detected key " + ch);
            }
            return ch + "\n";
        }

        @Override
        protected String performOutFilter(String ch) {
            return EMPTY_STRING;
        }

        @Override
        protected String getHeader() {
            return header;
        }

        @Override
        protected String getFooter() {
            return footer;
        }
    }

    /**
     * To remove all content inside the header and footer.
     *
     * @author tchemit &lt;chemit@codelutin.com&gt;
     * @since 1.1
     */
    public static class FragmentRemover extends DefaultFilter {

        /** Logger. */
        static private final Log log =
                LogFactory.getLog(FragmentRemover.class);

        protected String header;

        protected String footer;

        protected boolean verbose;

        public FragmentRemover(String header, String footer) {
            this.header = header;
            this.footer = footer;
        }

        public void setVerbose(boolean verbose) {
            this.verbose = verbose;
        }

        @Override
        protected String performInFilter(String ch) {
            if (verbose || log.isInfoEnabled()) {
                log.info("Will remove " + ch);
            }
            return EMPTY_STRING;
        }

        @Override
        protected String performOutFilter(String ch) {
            return ch;
        }

        @Override
        protected String getHeader() {
            return header;
        }

        @Override
        protected String getFooter() {
            return footer;
        }
    }

}
